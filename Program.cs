﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeNumbers
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int inputNumber;

            Console.WriteLine("Input your number:");
            inputNumber = Convert.ToInt32(Console.ReadLine());
            if (inputNumber == 0 || inputNumber == 1)
            {
                Console.WriteLine(inputNumber + " is not prime number");
                Console.ReadLine();
            }
            else
            {
                for (int ix = 2; ix <= inputNumber-1; ix++)
                {
                    var answer=inputNumber % ix;
                    if (answer == 0)
                    {
                        Console.WriteLine(inputNumber + "false not a prime number");
                        Console.ReadLine();
                        return;
                    }

                }
                Console.WriteLine(inputNumber + " prime number is true");
                Console.ReadLine();
            }

        }

        
    }
}
